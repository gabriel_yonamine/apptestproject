import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { isNgTemplate } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'projeto';

  apiURL = 'http://localhost:1337/usuarios';

  apiScoreUrl = 'http://localhost:1337/score-clientes';

  apiScoreData;

  apiData;
  
  constructor( private http:HttpClient){

  }

  

  ngOnInit(){
    this.http.get(this.apiURL).subscribe((data)=>{
      console.warn(data)
      this.apiData=data;
    })

    this.http.get(this.apiScoreUrl).subscribe((datascore)=>{
      console.warn(datascore)
      this.apiScoreData=datascore;
    })
  }

  onSubmit(data){
    this.http.post('http://localhost:1337/usuarios', data)
    .subscribe((result)=>{
      console.warn("result",result)
      this.ngOnInit();
    })
    
    
  }
}
